// See LICENSE for details

#ifndef spike_interface
#define spike_interface

#include <stdio.h>
#include <stdlib.h>

#include "sim.h"
#include "mmu.h"
#include "remote_bitbang.h"
#include "cachesim.h"
#include "extension.h"
#include <dlfcn.h>
#include <fesvr/option_parser.h>

#include <iostream>
#include <vector>
#include <string>
#include <memory>

typedef void*  c_spike_intf;

class spike_intf{
public:
  // Constructors
  spike_intf(std::string);
	// Destructor
  ~spike_intf();
	bool destroy_sim();	
	bool initialise(char* ISA);
	uint32_t single_step();
	uint64_t get_variable(uint32_t address);
	void set_variable(uint32_t address,uint64_t val);
  sim_t* s;
  int ret_num();
	std::string spike_args;
};

#endif
