"""
    Module for getting the constant addresses of the respective register in the
    design
"""



# /////////////////////////# Register Mapping for Machine Mode Regs /////////////////
ISA = 'RV64IMAFDC'
PC = 4096+32
XPR = 0x1000
FPR = 0x1021
PRIV = 0x1041
MSTATUS	= 0x300 # Machine Status register                                
MISA		= 0x301 # ISA and extensions                                     
MEDELEG	= 0x302 # Machine exception delegation                               
MIDELEG	= 0x303 # Machine interrupt delegation                               
MIE			= 0x304 # Machine interrupt enable                                   
MTVEC		= 0x305 # Machine trap-handler base address                          
MCOUNTEREN  = 0x306 # Machine counter setup register                                  
MSCRATCH	= 0x340 # Scratch rgister for machine trap hanglers                  
MEPC			= 0x341 # Machine exception program counter                          
MCAUSE		= 0x342 # Machine trap cause                                         
MTVAL		  = 0x343 # Machine bad address                                        
MIP			  = 0x344 # Machine interrupt pending
MCYCLE		= 0xB00 # Machine cycle counter                                      
MTIME		  = 0xB01	# mtime register (Non-standard r/w)
MINSTRET	= 0xB02 # Machine instructions retired.                              
MTIMECMP	= 0xB20 #  time compare register (Non-standard r/w)
MCYCLEH	  = 0xB80 # Upper 32 bits of mcycle                                   
MTIMEH		= 0xB81	# mtime hi-register (Non-standard r/w)
MINSTRETH = 0xB82 # Upper 32 bits of minstret.                                 
MTIMECMPH = 0xBA0 #  time compare hi-register (Non-standard r/w)
MVENDORID = 0xF11 # Vendor ID                                                  
MARCHID	  = 0xF12 # Architecture ID                                           
MIMPID		= 0xF13 # Implementation ID                                        
MHARTID		= 0xF14 # Hardware Thread ID                                      
# ////# Reister Mapping for User Mode Regs /////////////////
USTATUS	  = 0x000 # User status register
FFLAGS		= 0x001 # FP Accrued exceptions
FRM			  = 0x002 # FP Dynamic rounding mode
FCSR			= 0x003 # FP Control and status register
UIE			  = 0x004 # User interrupt enable register
UTVEC		  = 0x005 # User trap handler base address
USCRATCH	= 0x040 # Scratch register for user trap handlers
UEPC			= 0x041 # User exception program counter
UCAUSE		= 0x042 # User trap cause
UTVAL		  = 0x043 # User bad address or illegal instruction
UIP			  = 0x044 # User interrupt pending
UMEMSE		= 0x045 # Machine Memory Structures enable
UCYCLE		= 0xC00 # cycle counter for RDCYCLE instruction.
UTIME		  = 0xC01 # Tiemr for RDTIME instruction
UINSTRET	= 0xC02 # Instruction retired counter for RDINSTRET
UCYCLEH	  = 0xC80 # Upper 32bits of UCYCLE
UTIMEH		= 0xC81 # Upper 32bits of UTIME
UINSTRETH = 0xC82 # Upper 32bits of UINSTRET
# /////////////////////////# Register Mapping for Supervisor Mode Regs /////////////////
SSTATUS	  = 0x100 # Supervisor Status register                                
SEDELEG   = 0x102 # Supervisor exception delegation register
SIDELEG   = 0x103 # Supervisor interrupt delegation register
SIE       = 0x104 # Supervisor interrupt enable register
STVEC	    = 0x105 # Supervisor trap vector register
SSCRATCH  = 0x140 # Supervisor scratch register
SEPC      = 0x141 # Supervisor exception program counter
SCAUSE    = 0x142 # Supervisor cause register
STVAL     = 0x143 # Supervisor bad address
SIP       = 0x144 # Supervisor interrupt pending
SATP      = 0x180 # Supervisor address translation and protection
# ////////////////////////////////////////////////////////////////////////////////////
# ///////////////////////////# Physical Memory Protection ///////////////////////////
PMPCFG0     = 0x3A0
PMPCFG1     = 0x3A1
PMPCFG2     = 0x3A2
PMPCFG3     = 0x3A3
PMPADDR0    = 0x3B0
PMPADDR1    = 0x3B1
PMPADDR2    = 0x3B2
PMPADDR3    = 0x3B3
PMPADDR4    = 0x3B4
PMPADDR5    = 0x3B5
PMPADDR6    = 0x3B6
PMPADDR7    = 0x3B7
PMPADDR8    = 0x3B8
PMPADDR9    = 0x3B9
PMPADDR10    = 0x3BA
PMPADDR11    = 0x3BB
PMPADDR12    = 0x3BC
PMPADDR13   = 0x3BD
PMPADDR14    = 0x3BE
PMPADDR15    = 0x3BF
# ////////////////////////////////////////////////////////////////////////////////////
DCSR			= 0x7b0 # Debug CSR
DPC				= 0x7b1 # Debug PC
DSCRATCH  = 0x7b2 # Debug Scratch
DTVEC     = 0x7C0 # Debug trap vector
DENABLE   = 0x7C1 # Debug enable register
# ////////////////////////////////////////////////////////////////////////////////////
TSELECT   = 0x7a0
TDATA1    = 0x7a1
TDATA2    = 0x7a2
TDATA3    = 0x7a3
TINFO     = 0x7a4
TCONTROL  = 0x7a5
TMCONTEXT = 0x7a8
TSCONTEXT = 0x7aa
# ////////////////////////# Non-Standard User RW CSRS ///////////////////////////////
CUSTOMCNTRL = 0x800 # custom control register

