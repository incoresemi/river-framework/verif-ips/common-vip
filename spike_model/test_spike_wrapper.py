import sys

import spike_intf

print('ISA: {0}'.format(sys.argv[1]))
print('TEST: {0}'.format(sys.argv[2]))

PC = 0x1020
XPR = 0x1000
FPR = 0x1021
PRIV = 0x1041

ifc = spike_intf.spike_intf(sys.argv[2])
print("All ok")
ifc.initialise(sys.argv[1])
print('pc: {0}'.format(ifc.get_variable(PC))) # pc
print('priv: {0}'.format(ifc.get_variable(PRIV))) # priv
print('x[0]: {0}'.format(ifc.get_variable(0x1000))) # XPR_BASE
print('x[1]: {0}'.format(ifc.get_variable(0x1001))) # XPR_BASE
print('x[2]: {0}'.format(ifc.get_variable(0x1002))) # XPR_BASE
print('x[5]: {0}'.format(ifc.get_variable(0x1005))) # XPR_BASE
print('mstatus: {0}'.format(ifc.get_variable(0x300))) # mstatus
for i in range(100):
    ifc.single_step()
print('pc: {0}'.format(ifc.get_variable(0x1020))) # pc
print('priv: {0}'.format(ifc.get_variable(0x1041))) # priv
print('x[0]: {0}'.format(ifc.get_variable(0x1000))) # XPR_BASE
print('x[1]: {0}'.format(ifc.get_variable(0x1001))) # XPR_BASE
print('x[15]: {0}'.format(ifc.get_variable(0x1015))) # XPR_BASE
print('x[5]: {0}'.format(ifc.get_variable(0x1005))) # XPR_BASE
print('mstatus: {0}'.format(ifc.get_variable(0x300))) # mstatus


