// See LICENSE for details

#include "spike_intf.hpp"

#include <iostream>
#include <vector>
#include <string>

#define PRIV 0x1041                                               
#define XPR_REG 0x1000                                                                   
#define DEFAULT 0xAAAAAAAAAAAAAAAA
#define pc_value 32           
#define prv_value 65          
#define fpr_value 33          
#define no_of_bits 16         
#define hart_add 0xFFFF0000   
#define stateid_add 0x0000FFFF

void sim_t::main()  
{ 
  step(1);
  //if (remote_bitbang) remote_bitbang->tick();
}
/*
void sim_t::set_log(bool value){
  step(1);
  log = false; // disable log printing
}
*/
void sim_t::configure_log(bool enable_log, bool enable_commitlog){
   step(1);
   log = false;	//disable log printing
}

void sim_t::step(size_t n)
{
  //if (remote_bitbang) remote_bitbang->tick(); // This was added here  it is functionally eqv
  for (size_t i = 0, steps = 0; i < n; i += steps)
  {
    steps = std::min(n - i, INTERLEAVE - current_step);
    procs[current_proc]->step(steps);

    current_step += steps;
    if (current_step == INTERLEAVE)
    {
      current_step = 0;
      procs[current_proc]->get_mmu()->yield_load_reservation();
      if (++current_proc == procs.size()) {
        current_proc = 0;
        clint->increment(INTERLEAVE / INSNS_PER_RTC_TICK);
      }
      //host->switch_to(); // This affects the ability of spike to terminate on infinite loops.
      // There are 2 contexts one host target , There is current an error with some sort of improper initialisation
      // which prints an error message when host->switch_to() is called . // figure out that for htif periphral compatibility.
    }
  }
}

void sim_thread_main(void* arg) //  ref:: sim_t ::main()
{
  ((sim_t*)arg)->main();
}

int sim_t::run()  // This method just initialises and loads the binary.
{
  host = context_t::current();
  target.init(sim_thread_main, this);
  htif_t::start();
  return 0;
}
// The spike_intf Transport Interface module Members defined 
spike_intf::spike_intf(std::string elf_name){
  spike_args = elf_name;
  std::cout << "Spike model created" << std::endl ;
  return;
}
spike_intf::~spike_intf() {
  std::cout << "Spike model deleted" << std::endl ;
  return;
}

bool spike_intf::destroy_sim(){
  if(s != NULL){
    s->stop();
    delete s;
    return true;
  }
  return false;
}

static std::vector<std::pair<reg_t, mem_t*>> make_mems(const char* arg)
{
  // handle legacy mem argument
  char* p;
  auto mb = strtoull(arg, &p, 0);
  if (*p == 0) {
    reg_t size = reg_t(mb) << 20;
    if (size != (size_t)size)
      throw std::runtime_error("Size would overflow size_t");
    return std::vector<std::pair<reg_t, mem_t*>>(1, std::make_pair(reg_t(DRAM_BASE), new mem_t(size)));
  }

  // handle base/size tuples
  std::vector<std::pair<reg_t, mem_t*>> res;
  while (true) {
    auto base = strtoull(arg, &p, 0);
    auto size = strtoull(p + 1, &p, 0);
    res.push_back(std::make_pair(reg_t(base), new mem_t(size)));
    if (!*p)
      break;
    arg = p + 1;
  }
  return res;
}

bool spike_intf::initialise(char* ISA) {
  bool debug = false;
  bool halted = false;
  bool histogram = false;
  bool log = false;
  bool dump_dts = false;
  bool dtb_enabled = true;
  size_t nprocs = 1;
  reg_t start_pc = reg_t(-1);
  std::vector<std::pair<reg_t, mem_t*>> mems;
  std::vector<std::pair<reg_t, abstract_device_t*>> plugin_devices;
  std::unique_ptr<icache_sim_t> ic;
  std::unique_ptr<dcache_sim_t> dc;
  std::unique_ptr<cache_sim_t> l2;
  bool log_cache = false;
  bool log_commits = false;
  std::function<extension_t*()> extension;
  const char* isa = DEFAULT_ISA;
  const char* priv = DEFAULT_PRIV;
  const char* varch = DEFAULT_VARCH;
  uint16_t rbb_port = 0;
  bool use_rbb = false;
  unsigned dmi_rti = 0;
  bool real_time_clint = false;
  reg_t initrd_start = 0;
  reg_t initrd_end = 0;
  const char* bootargs;
  const char *log_path = nullptr;
  const char *dtb_file = NULL;
  #ifdef HAVE_BOOST_ASIO
  boost::asio::io_service *io_service_ptr = NULL; // needed for socket command interface option -s
  tcp::acceptor *acceptor_ptr = NULL;
  #endif
  debug_module_config_t dm_config = {
    .progbufsize = 2,
    .max_bus_master_bits = 0,
    .require_authentication = false,
    .abstract_rti = 0,
    .support_hasel = true,
    .support_abstract_csr_access = true,
    .support_haltgroups = true
  };
  if(ISA != NULL){
	  isa = (const char*)ISA;
  }
  int argc;
  char **argv;
  option_parser_t parser;
  //auto argv1 = parser.parse(argv);

  std::vector<int> hartids;
  //std::vector<std::string> htif_args(argv1, (const char*const*)argv + argc);
  std::vector<std::string> htif_args;

  //std::vector<std::string> htif_args;
  if (mems.empty())
    mems = make_mems("2048");

  //std::vector<std::string> htif_args;
  htif_args.push_back(spike_args);

/*  s = new sim_t(isa, priv, varch, nprocs, halted, start_pc, mems, plugin_devices, htif_args,
      std::move(hartids), dm_config);
 */
s = new sim_t(isa, priv, varch, nprocs, halted, real_time_clint,
             initrd_start, initrd_end, bootargs,
             start_pc, mems,
             plugin_devices,
             htif_args,
             hartids,
             dm_config,
             log_path,
             dtb_enabled, dtb_file
  #ifdef HAVE_BOOST_ASIO
             , io_service *io_service_ptr, tcp::acceptor *acceptor_ptr 
  #endif
	    );

  std::unique_ptr<remote_bitbang_t> remote_bitbang((remote_bitbang_t *) NULL);
  std::unique_ptr<jtag_dtm_t> jtag_dtm(
      new jtag_dtm_t(&s->debug_module, dmi_rti));
  if (use_rbb) {
    remote_bitbang.reset(new remote_bitbang_t(rbb_port, &(*jtag_dtm)));
    s->set_remote_bitbang(&(*remote_bitbang));
  }
  //s->set_dtb_enabled(dtb_enabled);
  //s->dtb_enabled;

  if (dump_dts) {
    printf("%s", s->get_dts());
    return 0;
  }

  if (ic && l2) ic->set_miss_handler(&*l2);
  if (dc && l2) dc->set_miss_handler(&*l2);
  if (ic) ic->set_log(log_cache);
  if (dc) dc->set_log(log_cache);
  for (size_t i = 0; i < nprocs; i++)
  {
    if (ic) s->get_core(i)->get_mmu()->register_memtracer(&*ic);
    if (dc) s->get_core(i)->get_mmu()->register_memtracer(&*dc);
    if (extension) s->get_core(i)->register_extension(extension());
  }

  s->run(); // Essentially initialises everything 
//  s->set_debug(debug);
//  s->set_log(log);
//  s->set_histogram(histogram);
//  s->set_log_commits(log_commits);

  return true;
}


uint32_t spike_intf::single_step() {
	uint32_t event = 0; //ALL_OK; // ref events.hpp	 // ALL OK is Equal to 0
  //s->set_log(0);
  s->configure_log(0, 0);
	return event;
}

void spike_intf::set_variable(uint32_t address,uint64_t val){
  uint32_t hart  = (address & hart_add) >>no_of_bits ; 
  uint32_t stateid = address & stateid_add;
  if (hart != 0 )return;
  if (stateid > (PRIV) )return;
  processor_t* HART =s->get_core(hart); // This is where Im locking this down to one core
  //state_t* hartstate = HART->get_state();
//Pointing to set_csr for values less than XPR_REG
	if(stateid<XPR_REG){
    HART->set_csr(stateid,val);
  } 
  return;
}

uint64_t spike_intf::get_variable(uint32_t address){
  uint64_t value = DEFAULT;
  uint32_t hart  = (address & hart_add) >>no_of_bits ; 
  uint32_t stateid = address & stateid_add;
  
  //std::cout << hart << stateid << std::endl ;  // debug

  // This CODE is FRAGILE and WILL Break for an INVALID ADDRESS PLEASE 
  // Fragile Interface Safeguard start 
  if (hart != 0 )return value;
  if (stateid > (PRIV) )return value;
  // Fragile Interface Safeguard end 

  processor_t* HART =s->get_core(hart); // This is where Im locking this down to one core
  state_t* hartstate = HART->get_state();

  //This can select CSR Registers with value less than XPR_REG
	if(stateid<XPR_REG){
    value = HART->get_csr(stateid);
    return value;
  }
  stateid = stateid - XPR_REG;	//Logic for Register with values between XPR_REG and PRIV
  if (stateid < pc_value){
    value = hartstate->XPR[stateid];
  }
  else if(stateid==pc_value)value = hartstate->pc; 
  else if(stateid==prv_value)value = hartstate->prv; 
  else {
     //uint64_t softfloat_cast<float128_t, uint64_t>(const float128_t &v)
    float128_t lv_value = hartstate->FPR[stateid-fpr_value];	//For FPR Register
    value = *(reinterpret_cast<uint64_t*>(&lv_value));
    // the only thing left is FPR as GPR CSR adn PC and FRAGILE take care of everything else
//    value = static_cast<uint64_t>(lv_value);
  }
  return value;
}

void* spike_init(char* String,char* ISA, char* elf_name){
  spike_intf* A = new spike_intf(std::string(elf_name));
  ((spike_intf*)(A))->spike_args = std::string(String);
  ((spike_intf*)(A))->initialise(ISA);
  //return reinterpret_cast<void*>(A);
  return (void*)A;
}

void set_variable(c_spike_intf instance,uint32_t address,uint64_t val){
  c_spike_intf A = reinterpret_cast<c_spike_intf*>(instance);
  ((spike_intf*)(A))->set_variable(address,val);
  return;
  }

unsigned int single_step(c_spike_intf instance){
  c_spike_intf A = reinterpret_cast<c_spike_intf*>(instance);
  ((spike_intf*)(A))->single_step();
  return 0;
}

unsigned long long int get_variable(c_spike_intf instance,unsigned int SLSVaddress){
  c_spike_intf A = reinterpret_cast<c_spike_intf*>(instance);
  return ((spike_intf*)(A))->get_variable(SLSVaddress);
}

void destroy(c_spike_intf instance){
  delete reinterpret_cast<c_spike_intf*>(instance);
  return;
}

int spike_intf::ret_num()
{
  return 5;
}
