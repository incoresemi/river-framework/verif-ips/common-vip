/* See LICENSE for details */

%module spikeintf
%{
  #include "spike_intf.hpp"
%}

/*In Built Interface FIles */
%include "stdint.i"
%include "std_vector.i"
%include "std_pair.i"
%include "typemaps.i"
%include "std_string.i"


class spike_intf{
public:
  // Constructors
  spike_intf(std::string);
	// Destructor
  ~spike_intf();
	bool destroy_sim();	
	bool initialise(char* ISA);
	uint32_t single_step();
	uint64_t get_variable(uint32_t address);
	void set_variable(uint32_t address,uint64_t val);
  sim_t* s;
	std::string spike_args = "file_not_initialized";
};

